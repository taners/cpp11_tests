#ifndef _CLASSES_HPP_
#define _CLASSES_HPP_

class Classes
{
public:
    Classes();
    Classes(const int _x);
    Classes(const Classes&); //copy constructor

    void SetMember(const int _x);
    int GetValue() {return member;}

    Classes& GetObjR();
    Classes GetObj();
    
private:
    int member;
};

#endif