#ifndef _CLASSES_HPP_
#include "classes.hpp"
#endif

#include <cstdio>

Classes::Classes()
{
    printf("Default constructor \n");
    member = 0;
}

Classes::Classes(const int _x)
{
    printf("const int constructor \n");
    member = _x;
}

Classes::Classes(const Classes& _obj)
{
    //the copy ctor
    printf("Copy constructor \n");
    member = _obj.member;
}


void Classes::SetMember(const int _x)
{
    member = _x;
}

Classes& Classes::GetObjR()
{
    Classes* x = new Classes(50);
    return *x;
}

Classes Classes::GetObj()
{
    Classes x(100);
    return x;
}

int main()
{
    printf("\nInit c0...\n");
    Classes c0;
    printf("c0.member = %d \n",c0.GetValue());

    printf("\nInit c1...\n");
    Classes c1(5);
    printf("c1.member = %d \n",c1.GetValue());

    printf("\nInit c2...\n");
    Classes c2 = c1.GetObj();
    printf("c2.member = %d \n",c2.GetValue());

    printf("\nInit c3...\n");
    Classes c3 = c1.GetObjR(); // GetObjR calls int constructor
    printf("c3.member = %d \n",c3.GetValue());

    return 0;
}