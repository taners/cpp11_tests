#include <cstdio>

int main(void)
{
  const char* s_arry[5] = {"zero", "one", "two", "three", "four"};
  const int i_arry[5] = {11, 22, 33, 44, 55};
  int i;

  printf("Which element? ");
  scanf("%d",&i);
  printf("Element %d: %s \n", i, s_arry[i]);
  printf("Element %d: %d \n", i, i_arry[i]);

  printf("--------------------------\n");

  printf("First int element: %d \n", *i_arry);
  printf("Second int element: %d \n", *(i_arry+1));
  printf("--------------------------\n");

  printf("First char* element: %s \n", *s_arry);
  printf("Third char* element: %s \n", *(s_arry+2));

  return 0;

}