# Declare a variable for the compliler which we are going to use
CC=g++
# Declare another variable with the flags which we are going to use
CFLAGS=-c -Wall -std=c++11 -pthread
OBJDIR=bin

all: clean classes simple_thread

simple_thread: simple_thread.o
	$(CC) $(OBJDIR)/simple_thread.o -o $(OBJDIR)/simple_thread

simple_thread.o: classes simple_thread.cpp
	$(CC) $(CFLAGS) simple_thread.cpp -o $(OBJDIR)/simple_thread.o

classes: classes.o
	$(CC) $(OBJDIR)/classes.o -o $(OBJDIR)/classes

classes.o: classes.hpp sandbox
	$(CC) $(CFLAGS) -c classes.cpp -o $(OBJDIR)/classes.o

sandbox: 
	$(CC) $(CFLAGS) sandbox.cpp -o $(OBJDIR)/sandbox.o
	$(CC) $(OBJDIR)/sandbox.o -o $(OBJDIR)/sandbox

clean:
	- rm -rf $(OBJDIR)
	- mkdir -p $(OBJDIR)
