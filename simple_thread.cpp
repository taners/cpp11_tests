#include <iostream>
#include <thread>

void foo(int start, int n) {
    // foo will print the given range
    for(int i = start; i < n; i++){
        printf("[ %d ] \n", i);
    }
}


int main() {
    // Check utf8 compliance
    std::string my_str = "Здраво!";
    std::cout << my_str << std::endl;
    // Launch a thread. I.e. run void foo() in a new thread
    std::thread thread1(foo,1,10);
    // Here we use bind function to bind the arguments to foo explicitly
    std::thread thread2(std::bind(foo,101,110));
    // Join the thread with the main thread. This means wait for it to finish.
    // We want this in case main thread finished before foo in which case it would kill 
    // foo thread wheather or not it has finished!
    thread1.join();
    thread2.join();

    // Explicit return is always good practice
    return 0;
}